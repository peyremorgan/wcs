### To check

- [ ] Declare all new angular proxy components in the file angular/projects/wcs-angular/src/lib/wcs-angular.module.ts
  for newly created web components
- [ ] Explain what has changed in the changelog
